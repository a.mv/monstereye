import logging
from nio import (
	SendRetryError
)
from markdown import markdown

LOG = logging.getLogger("MonsterEye")


# from html.parser import HTMLParser
# class HTMLFilter(HTMLParser):
# 	text = ""
#
# 	def handle_data(self, data):
# 		self.text += data

class Chat():

	def __init__(self, client, default_room):
		self.client = client
		self.default_room_id = self.get_room_id(default_room)

	def get_room_id(self, room):
		for roomid in self.client.rooms:
			if self.client.rooms[roomid].named_room_name() == room:
				return roomid
		LOG.error('Cannot find id for room %s - is the bot on it?', room)
		return None

	def get_direct_chat_id(self, user):
		chat = None
		for roomid in self.client.rooms:
			if self.client.rooms[roomid].is_group and user in self.client.rooms[roomid].users:
				chat = roomid
		if chat is None:
			LOG.error('Cannot find direct chat with user %s - is the bot on it?', user)
		return chat

	async def send_text_to_room(
			self,
			message,
			room=None,
			room_id=None,
			notify=False,
			html=True,
			markdown_convert=True
	):
		"""Send text to a matrix room

		Args:
			client (nio.AsyncClient): The client to communicate to matrix with

			room_id (str): The ID of the room to send the message to

			message (str): The message content

			notify (bool): Whether the message should be sent with an "m.notice" message type
				(will not ping users)

			markdown_convert (bool): Whether to convert the message content from markdown.
				Defaults to true.
		"""
		if room_id is None:
			if room is None:
				room_id = self.default_room_id
			else:
				room_id = self.get_room_id(room)
				if room_id is None:
					room_id = self.default_room_id
		if room_id is None:
			return
		# Determine whether to ping room members or not
		msgtype = "m.text" if notify else "m.notice"

		# f = HTMLFilter()
		# f.feed(message)
		# content["body"] = f.text

		await self.room_send(message,msgtype,room_id,html,markdown_convert)

	async def send_text_to_user(
			self,
			message,
			user_id,
			notify=True,
			html=True,
			markdown_convert=True
	):
		# Determine whether to ping room members or not
		msgtype = "m.text" if notify else "m.notice"
		room_id = self.get_direct_chat_id(user_id)
		if room_id is None:
			return
		LOG.debug(f"Room {room_id} found with user {user_id}")
		await self.room_send(message,msgtype,room_id,html,markdown_convert)

	async def room_send(
			self,
			message,
			msgtype,
			room_id,
			html=True,
			markdown_convert=True
	):
		content = {
			"msgtype": msgtype,
			"body": message
		}
		if html:
			content["format"] = "org.matrix.custom.html"
			if markdown_convert:
				content["formatted_body"] = markdown(message, extensions=['tables', 'fenced_code'])
			else:
				content["formatted_body"] = message
		else:
			content["body"] = message
		try:
			await self.client.room_send(
				room_id,
				"m.room.message",
				content,
			)
		except SendRetryError:
			LOG.exception(f"Unable to send message response to {room_id}")
