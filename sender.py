#!/usr/bin/python3
import os
import locale
import sys
import email
import select
import argparse
import logging

from config import Config
import asyncio
import json

os.environ["PYTHONIOENCODING"] = "utf-8"
myLocale = locale.setlocale(category=locale.LC_ALL, locale="en_GB.UTF-8")
LOG = logging.getLogger("MonsterEye")


async def send_message(socket, message):
    # reader, writer = await asyncio.open_connection('127.0.0.1', 9123,loop=loop)
    reader, writer = await asyncio.open_unix_connection(socket)
    for line in message:
        writer.write(line.encode())
    await writer.drain()
    writer.close()


########################################################################################################
# -------------------------------------------------------------------------------------------------------
# Setup the command line arguments.
optp = argparse.ArgumentParser(description='Monster Eye Sender')

# Output verbosity options.
optp.add_argument('bot_config', help='Daemon name from config file')
optp.add_argument('-d', '--debug', help='set logging to DEBUG',
                  action='store_const', dest='loglevel',
                  const=logging.DEBUG, default=logging.ERROR)
optp.add_argument('-v', '--verbose', help='set logging to COMM',
                  action='store_const', dest='loglevel',
                  const=logging.INFO, default=logging.ERROR)
optp.add_argument('-q', '--quiet', help='set logging to NONE',
                  action='store_const', dest='loglevel',
                  const=logging.CRITICAL, default=logging.ERROR)
optp.add_argument('-n', '--notify', help='Always notify recipient',
                  action='store_true', dest='notify')
optp.add_argument('-t', '--to', help='Message destination',
                  action='store', dest='to_addr')
optp.add_argument('-s', '--subject', help='Message subject',
                  action='store', dest='subject')
optp.add_argument('-c', '--config', help='Config file',
                  action='store', dest='config')

opts = optp.parse_args()
# Setup logging.
logging.basicConfig(level=opts.loglevel,
                    format='MonsterEye.Sender : %(levelname)-8s %(message)s')

if opts.config is None:
    opts.config = Config.getLocalFile("watcher.yaml")

config = Config(opts.config, opts.bot_config)

msg = None
if select.select([sys.stdin], [], [], 0)[0]:
    msg = sys.stdin.read()
else:
    sys.stderr.write("No message found on standard input\n")
    sys.exit(-1)
mail = email.message_from_string(msg)
body = mail.get_payload()
subject = mail.get('Subject')

if opts.subject is not None:
    subject = opts.subject
elif subject is not None:
    subject = email.header.decode_header(subject.strip().replace("\n", ""))
    if isinstance(subject, list):
        r = []
        for item in subject:
            if item[1] is not None:
                r.append(item[0].decode(item[1]))
            else:
                r.append(item[0])
        subject = ' '.join(r)

destination = None
if opts.to_addr is not None:
    destination = opts.to_addr
else:
    to = mail.get("X-Original-To")
    if to is not None:
        # Try to convert mail to matrix
        try:
            # to = "@{0}:{1}".format(to.split('@')[0], config.HomeDomain)
            to = "@{0}".format(to.split('@')[0])
        except:
            pass
        if to is not None:
            destination = to
if destination is None:
    destination = f"#{config.room}"
message = {
    "to": destination,
    "notify": opts.notify,
    "body": []
}
if subject is not None:
    message["body"].append(f"#### {subject}  \n")
if body is None:
    message["body"].append(msg)
else:
    if mail.is_multipart():
        for part in body:
            message["body"].append('```')
            message["body"].append(part)
            message["body"].append('```')
            message["body"].append("---\n")
    else:
        message["body"].append("```")
        message["body"].append(body)
        message["body"].append("```")

LOG.debug(json.dumps(message, indent=4))
loop = asyncio.get_event_loop()
loop.run_until_complete(send_message(config.pipe, json.dumps(message).split("\n")))
loop.close()

sys.exit(0)
