import json
import logging
from json import JSONDecodeError

from jsonschema import validate
from jsonschema.exceptions import *
from config import Config

LOG = logging.getLogger("MonsterEye")


class MessageProcessor():
    def __init__(self, schema='message_schema.json'):
        self.message = None
        schema_file = Config.getLocalFile(schema)
        with open(schema_file, 'r') as file:
            self.schema = json.load(file)

    def load(self, message):
        try:
            json_data = json.loads(message)
            validate(instance=json_data, schema=self.schema)
        except SchemaError as e:
            LOG.error("There is an error with the schema")
            LOG.error(e)
        except ValidationError as e:
            self.message = None
            LOG.error(e)
        except JSONDecodeError as e:
            self.message = None
            LOG.error(e)
            LOG.debug(message)
        else:
            self.message = json_data
        return self.message


if __name__ == "__main__":
    with open("test_message.json", 'r') as file:
        message = "".join(file.readlines())
    processor = MessageProcessor()
    validated = processor.load(message)
    if validated is not None:
        print("Good message")
        print(validated["body"])
