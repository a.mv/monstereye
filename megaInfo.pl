#!/usr/bin/env perl
use strict;
use warnings;

open(my $fh, '-|', '/usr/local/bin/sudo /usr/local/sbin/MegaCli -ShowSummary -aAll') or die $!;
while (<$fh>) {

    if(/Controller/) {
	while(<$fh>) {
	    if(/BBU/) { last; }
	    elsif (/Status/) { print "Controller ".$_."</br>";}
	}
    } elsif (/PD/) { 
	print "Physical Drives\n";
	while(<$fh>) {
	    if(/Connector.*(Slot\s+\d+)/){
		print $1;
		while(<$fh>) {
		    if(/Power State\s+:\s+(.*)/) { print ",".$1;last;}
		    elsif(/State\s+:\s+(.*)/) { print " : ".$1; }
		}
		print "\n";
	    } elsif (/Virtual Drives/) { last; }
	}
    } elsif(/Virtual drive/) { 
    print "\n========\n\n";
    print;
	while(<$fh>) {
	    if(/Exit Code:/) {last;} 
	    else {print;}
	}
    }
}