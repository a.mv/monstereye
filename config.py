import logging
import re
import os
import sys
from urllib.parse import urlparse

import yaml

from access_token import AccessToken
from errors import ConfigError
from storage import Storage

LOG = logging.getLogger("MonsterEye")
VERSION = "1.1"
PYTHON_VERSION = "Python {}.{}".format(sys.version_info.major, sys.version_info.minor)

def warnExe(k, fpath):
	if " " in fpath:
		(path, args) = fpath.split(" ", 1)
	else:
		path = fpath
		args = ""
	path = Config.getLocalFile(path)
	if path is not None and not Config.IsExecutable(path):
		LOG.warning("%s %s is not executable !", k, path)
		return None
	return " ".join((path,args))


class Config(object):
	def __init__(self, filepath, bot_config):
		"""
		Args:
			filepath (str): Path to config file
		"""
		self.isDaemon = False
		self.botName = bot_config
		if not os.path.isfile(filepath):
			raise ConfigError(f"Config file '{filepath}' does not exist")

		# Load in the config file at the given filepath
		with open(filepath) as file_stream:
			config = yaml.full_load(file_stream.read())

		# Matrix bot account setup
		matrix = config.get("matrix", {})[bot_config]
		internal = config.get("internal", {})

		self.PID = matrix.get("PID")
		if not self.PID:
			raise ConfigError("PID is a required field")

		self.user_id = matrix.get("user_id")
		if not self.user_id:
			raise ConfigError("matrix.user_id is a required field")
		elif not re.match("@.*:.*", self.user_id):
			raise ConfigError("matrix.user_id must be in the form @name:domain")
		self.full_name = matrix.get("full_name")

		try:
			self.nick = re.search('@(.+?):', self.user_id).group(1)
		except AttributeError:
			self.nick = "Anonymous"

		self.NotifyUsers = matrix.get("NotifyUsers")
		uri = urlparse(matrix.get("homeserver"))
		self.HomeDomain = uri.netloc.split(":")[0]

		# self.access_token = matrix.get("access_token")
		# if not self.access_token:
		# 	raise ConfigError("matrix.access_token is a required field")

		self.device_id = matrix.get("device_id")
		if not self.device_id:
			LOG.warning(
				"Config option matrix.device_id is not provided, which means "
				"that end-to-end encryption won't work correctly"
			)

		self.homeserver = matrix.get("homeserver")
		if not self.homeserver:
			raise ConfigError("matrix.homeserver is a required field")

		self.command_re = internal.get("command_re", "(.*)")
		self.prompt_prefix = internal.get("prompt_prefix", "")
		self.sync_interval = internal.get("sync_interval", 5)
		self.help_text = internal.get("help_text", "Help ?")

		self.pipe = matrix.get("pipe")
		self.room = matrix.get("defaultRoom")

		commands = matrix.get("commands", {})
		self.exe = dict()
		for k in commands.keys():
			self.exe[k] = warnExe(k,commands[k])
			LOG.debug("%s ~ %s", k, self.exe[k])

		# Database setup
		self.database_filepath = matrix.get("db_store")
		if not self.database_filepath:
			LOG.warning("db_store is missing from configuration")
		else:
			# Configure the database
			self.storage = Storage(self)
			self.access_store = AccessToken(self.storage)
			self.store_dir = os.path.dirname(self.database_filepath)

	@staticmethod
	def IsExecutable(fpath):
		return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

	@staticmethod
	def getLocalFile(name):
		if name is None:
			return None
		else:
			this_dir = os.path.dirname(os.path.realpath(__file__))
			file = os.path.join(this_dir, name)
			if os.path.isfile(file):
				return file
			else:
				return name
