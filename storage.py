import sqlite3
import os.path
import logging
from threading import Lock
from dataclasses import dataclass

latest_db_version = 1

LOG = logging.getLogger("MonsterEye")


@dataclass
class Status(object):
	lock: Lock
	progress: str


class Storage(object):
	def __init__(self, config):
		"""Setup the database

		Runs an initial setup or migrations depending on whether a database file has already
		been created
		"""
		self.db_path = config.database_filepath
		self.status = dict()
		for k in config.exe.keys():
			self.status[k] = Status(Lock(), "")

		# Check if a database has already been connected
		if os.path.isfile(self.db_path):
			self._run_migrations()
		else:
			self._initial_setup()

	def _initial_setup(self):
		"""Initial setup of the database"""
		LOG.info("Performing initial database setup...")

		# Initialize a connection to the database
		try:
			self.conn = sqlite3.connect(self.db_path)
		except sqlite3.OperationalError as e:
			LOG.error("Database connection error : %s", e)
			raise e
		self.cursor = self.conn.cursor()

		# Sync token table
		self.cursor.execute("CREATE TABLE sync_token ("
		                    "dedupe_id INTEGER PRIMARY KEY, "
		                    "token TEXT NOT NULL"
		                    ")")
		# Login token table
		self.cursor.execute("CREATE TABLE access_token ("
		                    "dedupe_id INTEGER PRIMARY KEY, "
		                    "token TEXT NOT NULL"
		                    ")")

		LOG.info("Database setup complete")

	def _run_migrations(self):
		"""Execute database migrations"""
		# Initialize a connection to the database
		self.conn = sqlite3.connect(self.db_path)
		self.cursor = self.conn.cursor()
