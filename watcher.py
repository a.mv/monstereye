import sys, os
import threading

try:
    sys.path.append(os.environ["site-packages"])
except KeyError:
    pass
import argparse
import getpass
import logging.handlers
import asyncio
import signal
import re
from concurrent.futures import CancelledError

from aiohttp import ServerDisconnectedError, ClientConnectionError

from chat_functions import Chat
from json_message import MessageProcessor

from nio import (
    AsyncClient,
    AsyncClientConfig,
    RoomMessageText,
    InviteEvent,
    SyncError,
    LocalProtocolError)

from callbacks import Callbacks
from config import Config
from config import (VERSION,PYTHON_VERSION)
from daemon import Daemon
from errors import LoginError

LOG = logging.getLogger("MonsterEye")
LOGLEVEL_DEFAULT = logging.INFO


class Watcher:

    def __init__(self, config, deviceid=None):
        self.config = config
        self.client = None
        self.store = self.config.storage
        self.loop = None
        self.pipeserver = None
        self.loggedIn = False
        self.stopEvent = threading.Event()
        if deviceid is not None:
            self.deviceId = deviceid
        else:
            self.deviceId = self.__class__.__name__
        self.messageProcessor = MessageProcessor()

        # Configuration options for the AsyncClient
        client_config = AsyncClientConfig(
            max_limit_exceeded=0,
            max_timeouts=0,
            store_sync_tokens=True
        )

        # Initialize the matrix client
        self.client = AsyncClient(
            self.config.homeserver,
            self.config.user_id,
            device_id=self.deviceId,
            config=client_config,
            store_path=self.config.store_dir
        )


    @staticmethod
    def on_exception(loop, context):
        # context["message"] will always be there; but context["exception"] may not
        msg = context.get("exception", context["message"])
        LOG.error(f"Unhandled exception: {msg}")
        LOG.debug(context)

    async def on_signal(self, signal):
        LOG.info("Signal %s recived.", signal)
        if signal == 1:
            pass
        elif signal in [2, 15]:
            exit_text = f"{self.__class__.__name__} is exiting on signal {signal.name}."
            await self.chat.send_text_to_room(exit_text, notify=False, html=False)
            self.stopEvent.set()
            await self.client.close()
            LOG.info(exit_text)

    async def on_socket_connection(self, reader, writer):
        LOG.debug("Client connected")
        line = (await reader.readline()).decode("UTF-8").strip()
        if not line:
            return
        if not re.match("{", line):
            LOG.error("Wrong message format in pipe")
            LOG.debug(line)
            return

        LOG.debug("Got a message in pipe")
        lines = [line]
        while not reader.at_eof():
            #line = (await reader.readuntil(separator=b'}'))
            line = (await reader.readline()).decode("UTF-8").strip()
            lines.append(line)

        message = self.messageProcessor.load("".join(lines))
        LOG.debug(message)
        if message is None:
            LOG.error("Wrong message")
            return
        to = message['to']
        body = "\n".join(message['body'])
        if to == "all":
            if self.config.NotifyUsers is None:
                LOG.error("NotifyUsers is not set but used in the message")
            else:
                for d in self.config.NotifyUsers:
                    LOG.debug("Sending message to %s", d)
                    await self.chat.send_text_to_user(body, d, True, True, True)
        else:
            room = re.match("^#(\w+)", to)
            if room:
                LOG.debug("Sending message to room")
                await self.chat.send_text_to_room(body, room.group(1),notify=message['notify'])
            user = re.match("^@\w+", to)
            if user:
                username = f"{to}:{self.config.HomeDomain}"
                LOG.debug("Sending message to %s", username)
                await self.chat.send_text_to_user(body, username)

        LOG.debug("End of pipe...")
        return

    def set_callbacks(self):
        # Set up event callbacks
        callbacks = Callbacks(self.chat, self.store, self.config)
        self.client.add_event_callback(callbacks.on_message, (RoomMessageText,))
        self.client.add_event_callback(callbacks.on_invite, (InviteEvent,))

    def open_socket(self):
        coro = asyncio.start_unix_server(self.on_socket_connection, self.config.pipe)
        self.pipeserver = self.loop.run_until_complete(coro)
        LOG.info('Serving on {}'.format(self.pipeserver.sockets[0].getsockname()))
        os.chmod(self.config.pipe, 0o777)

    async def connect(self):
        self.loop = asyncio.get_event_loop()
        signals = (signal.SIGHUP, signal.SIGTERM, signal.SIGINT)
        for s in signals:
            self.loop.add_signal_handler(
                s, lambda s=s: self.loop.create_task(self.on_signal(s)))
        self.loop.set_exception_handler(self.on_exception)

        while not self.loggedIn:
            try:
                self.client.access_token = self.config.access_store.token # Sync token is stored in the internal store, but not access token
                sync_response = await self.client.sync(timeout=30000, full_state=not self.loggedIn)
                # Check if the sync had an error
                if type(sync_response) == SyncError:
                    if sync_response.status_code == "M_UNKNOWN_TOKEN":
                        LOG.error("%s : %s", sync_response.status_code, sync_response.message)
                    else:
                        LOG.warning("Error in client sync: %s", sync_response.message)
                        self.loggedIn = True
                else:
                    self.loggedIn = True
            except (ServerDisconnectedError, TimeoutError) as e:
                LOG.error(e)
                self.loggedIn = False
            except LocalProtocolError as e:
                LOG.error("LocalProtocolError : %s", e)
                self.loggedIn = False

            if not self.loggedIn:
                await self.client.close()
                try:
                    await self.getToken()
                except Exception as e:
                    raise e
                else:
                    self.loggedIn = True

        # self.config.room_id = get_room_id(self.client, self.config.room)
        self.chat = Chat(self.client, self.config.room)
        if self.config.full_name is not None:
            await self.client.set_displayname(self.config.full_name)

    # Sync loop
    # in_exception = False

    async def run(self):
        while self.loggedIn:
            try:
                # Sync encryption keys with the server
                # Required for participating in encrypted rooms
                if self.client.should_upload_keys:
                    await self.client.keys_upload()
                LOG.info("Watcher is running")
                await self.client.sync_forever(timeout=30000, full_state=True)
                if self.stopEvent.is_set():
                    LOG.debug("Sync interrupted and stop event is set : stop everything")
                    await self.stop()
                    return 0

            except (ClientConnectionError, ServerDisconnectedError):
                if not self.stopEvent.is_set():
                    LOG.warning(f"Unable to connect to homeserver, retrying in {config.sync_interval}s...")
                    # Sleep so we don't bombard the server with login requests
                    await asyncio.sleep(config.sync_interval)

            except TimeoutError as e:
                if not self.stopEvent.is_set():
                    LOG.info("Sync timeout : %s", e)
                    LOG.info("Retrying...")
                    await asyncio.sleep(config.sync_interval)

            except LocalProtocolError as e:
                LOG.error("LocalProtocolError : %s", e)
                break

            finally:
                if self.stopEvent.is_set():
                    await self.stop()
                    return 0
                else:
                    # Make sure to close the client connection on disconnect
                    await self.client.close()

        # Save the latest sync token to the database
        # token = sync_response.next_batch
        # if token:
        # 	sync_token.update(token)
        await self.stop()
        sys.exit(254)

    async def stop(self):
        if self.pipeserver is not None:
            self.pipeserver.close()
            await self.pipeserver.wait_closed()

        """Cleanup tasks tied to the service's shutdown."""
        if sys.version_info >= (3, 7):
            tasks = [t for t in asyncio.all_tasks() if t is not asyncio.current_task()]
        else:
            tasks = [t for t in asyncio.Task.all_tasks() if t is not asyncio.Task.current_task()]

        [task.cancel() for task in tasks]

        LOG.info(f"Cancelling {len(tasks)} outstanding tasks")
        await asyncio.gather(*tasks, return_exceptions=True)
        await self.client.close()
        #self.loop.stop()

    async def getToken(self):
        interactive = False
        try:
            password = os.environ.get('Password')
            if password is None:  # Going interactive
                interactive = True
                print(self.config.user_id)
                password = getpass.getpass()
        except Exception as error:
            LOG.error(error)
        else:
            # client = AsyncClient(self.config.homeserver, self.config.user_id, device_id=self.deviceId)
            await self.client.login(password, self.deviceId)
            if self.client.access_token is not None and self.client.access_token:
                if interactive:
                    print("Access token:", self.client.access_token)
                self.config.access_store.update(self.client.access_token)
                LOG.info("Login successful.")
            # await client.sync()
            else:
                raise LoginError("Login failed.")


class WatcherDaemon(Daemon):
    def __init__(self, watcher, debug=False):
        self.watcher = watcher
        super().__init__(self.watcher.config.PID, debug)

    def run(self):
        loop = asyncio.get_event_loop()
        MainLoop(loop, self.watcher)
        LOG.info("Daemon thread terminated")


def MainLoop(loop, watcher):
    LOG.info("Starting watcher v{}/{}".format(VERSION,PYTHON_VERSION))
    while True:
        try:
            loop.run_until_complete(watcher.connect())
            watcher.set_callbacks()
            watcher.open_socket()
        except KeyboardInterrupt:
            LOG.debug('MainLoop Interrupted')
            return 255
        except CancelledError:
            LOG.debug('MainLoop Cancelled')
            return 255
        except (LocalProtocolError, LoginError):
            LOG.error("Login failed")
            return 1
        except TimeoutError as e:
            LOG.info("Daemon timeout : %s", e)
            LOG.info("Restarting...")
            continue

        try:
            return loop.run_until_complete(watcher.run())
        except KeyboardInterrupt:
            LOG.debug('MainLoop Interrupted')
            return 255
        except CancelledError:
            LOG.debug('MainLoop Cancelled')
            return 255
        except (LocalProtocolError, TimeoutError) as e:
            LOG.info(e)
            LOG.info("Restarting...")
            continue


if __name__ == "__main__":
    ########################################################################################################
    # -------------------------------------------------------------------------------------------------------
    # Setup the command line arguments.
    optp = argparse.ArgumentParser(description='Monster Eye Watcher')

    # Output verbosity options.
    optp.add_argument('bot_config', help='Daemon name from config file')
    optp.add_argument('-l', '--login', help='Login to the Matrix',
                      action='store_true', dest='login')
    optp.add_argument('-q', '--quiet', help='set logging to ERROR',
                      action='store_const', dest='loglevel',
                      const=logging.ERROR, default=LOGLEVEL_DEFAULT)
    optp.add_argument('-d', '--debug', help='set logging to DEBUG',
                      action='store_const', dest='loglevel',
                      const=logging.DEBUG, default=LOGLEVEL_DEFAULT)
    optp.add_argument('-v', '--verbose', help='set logging to COMM',
                      action='store_const', dest='loglevel',
                      const=logging.INFO, default=LOGLEVEL_DEFAULT)
    optp.add_argument('-D', '--daemon', help='Start as daemon',
                      action='store', dest='daemon')

    optp.add_argument('-c', '--config', help='Config file',
                      action='store', dest='config')

    opts = optp.parse_args()

    formatter = logging.Formatter('[%(asctime)s: %(name)s.Watcher(v{0})]  %(levelname)-8s %(message)s'.format(VERSION))
    LOG.setLevel(opts.loglevel)
    if opts.daemon is not None or opts.loglevel == logging.ERROR:
        handler = logging.handlers.SysLogHandler(address='/dev/log', facility=logging.handlers.SysLogHandler.LOG_DAEMON)
    else:
        handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    LOG.addHandler(handler)

    configFile = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'watcher.yaml')
    if opts.config is not None:
        configFile = opts.config

    # Read config file
    config = Config(configFile, opts.bot_config)
    client = Watcher(config)

    if opts.daemon is not None:
        config.isDaemon = True
        LOG.info("Starting Daemon")
        daemon = WatcherDaemon(client, opts.loglevel != logging.ERROR)
        if 'start' == opts.daemon:
            daemon.start()
        elif 'stop' == opts.daemon:
            daemon.stop()
        else:
            print("usage: {} start|stop".format(sys.argv[0]))
            sys.exit(2)
        sys.exit(0)
    else:
        LOOP = asyncio.get_event_loop()
        exit_code = 0
        if opts.login:
            LOG.info("Login requested")
            LOOP.run_until_complete(client.getToken())
        else:
            exit_code = MainLoop(LOOP, client)
        LOG.debug("Exiting...")
        LOOP.run_until_complete(client.stop())
        exit(exit_code)
