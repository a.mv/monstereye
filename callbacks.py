import asyncio
import re
from commands import Command
from nio import (
        JoinError,
)

import logging
LOG = logging.getLogger("MonsterEye")


class Callbacks(object):

        def __init__(self, chat, store, config):
                """
                Args:
                        client (nio.AsyncClient): nio client used to interact with matrix

                        store (Storage): Bot storage

                        config (Config): Bot configuration parameters
                """
                self.chat = chat
                self.store = store
                self.config = config

        async def on_message(self, room, event):
                """Callback for when a message event is received

                Args:
                        room (nio.rooms.MatrixRoom): The room the event came from

                        event (nio.events.room_events.RoomMessageText): The event defining the message

                """
                # Extract the message text
                msg = event.body
                username = self.chat.client.user
                profile = await self.chat.client.get_profile(username)
                displayname = profile.displayname
                LOG.debug(
                        f"Bot message received for room {room.display_name} | "
                        f"{room.user_name(event.sender)}: '{msg}'"
                )

                if event.sender == self.chat.client.user:
                        LOG.debug("Ignore messages from ourselves")
                        return
                my_names=list()
                message_for_me = False
                for my_name in (self.config.nick, username,displayname):
                        my_names.append(my_name)
                        if my_name in msg:
                                message_for_me = True
                                LOG.debug(f"Message for '{my_name}'")
                if not message_for_me:
                        LOG.debug(f"Ignore messages not for me: {self.config.nick}/{username}/{displayname}")
                        return
                re_names = "|".join([re.escape(n) for n in my_names])
                command_re = re.compile(self.config.command_re.format(re_names))
                rq = command_re.match(msg)

                if (rq is not None):
                        command = rq.group(2)
                        if command is not None:
                                cmd = Command(self.chat, self.store, self.config, command, room, event)
                                asyncio.ensure_future(cmd.Process())
                        return # Command class will take care about feedback messsage
                else:
                        LOG.debug("Command not detected !")
                # message = Message(self.client, self.store, self.config, msg, room, event)
                # await message.process()
                if "hello" in msg.lower():
                        await self.chat.send_text_to_room("On'dabu.",room_id=room.room_id)


        async def on_invite(self, room, event):
                """Callback for when an invite is received. Join the room specified in the invite"""
                LOG.debug(f"Got invite to {room.room_id} from {event.sender}.")

                # Attempt to join 3 times before giving up
                for attempt in range(3):
                        result = await self.chat.client.join(room.room_id)
                        if type(result) == JoinError:
                                LOG.error(
                                        f"Error joining room {room.room_id} (attempt %d): %s",
                                        attempt, result.message,
                                )
                                if result.status_code == 'M_UNKNOWN':
                                        break
                                if result.status_code == 'M_LIMIT_EXCEEDED':
                                        LOG.info(f"Error joining room {room.room_id} too many requests, sleeping for %d ms",result.retry_after_ms)
                                        await asyncio.sleep(result.retry_after_ms/1000+1)
                                else:
                                        await asyncio.sleep(5)

                        else:
                                LOG.info(f"Joined {room.room_id}")
                                break
