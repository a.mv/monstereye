FILES = access_token.py callbacks.py chat_functions.py commands.py config.py daemon.py errors.py json_message.py megaInfo.pl message_schema.json requirements.txt sender.py storage.py sync_token.py watcher.py watcher.sudoers watcher.yaml
COMPRESS = tar -cJf 
OBJ_NAME = MonsterEye.tar.xz

all:dialog
debug: COMPILER_FLAGS = -w -g -DDEBUG
debug: dialog

#This is the target that compiles our executable
pkg : $(FILES)
	$(COMPRESS) $(OBJ_NAME) $(FILES)
