import asyncio
import inspect
import subprocess
import re
import shlex
import sys
from _datetime import datetime
import logging
LOG = logging.getLogger("MonsterEye")

from errors import ReturnCodeException

import psutil

class Command(object):
	def __init__(self, chat, store, config, command, room, event):
		"""A command made by a user

		Args:
			client (nio.AsyncClient): The client to communicate to matrix with

			store (Storage): Bot storage

			config (Config): Bot configuration parameters

			command (str): The command and arguments

			room (nio.rooms.MatrixRoom): The room the command was sent in

			event (nio.events.room_events.RoomMessageText): The event describing the command
		"""
		self.chat = chat
		self.store = store
		self.config = config
		self.room = room
		self.event = event
		self.loop = asyncio.get_event_loop() # It will be reset with the first call to process.

		self.args = command.split()[1:]
		self.command = command.split()[0]

		self.message = {
			"wrong_command": "!# Me not that kind of orc!",
			"exception": "!# Hey, what are these letters burned on my ass?  \n{}"
		}

	@staticmethod
	def _size(nbytes):
		suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
		if nbytes == 0: return '0 B'
		i = 0
		while nbytes >= 1024 and i < len(suffixes)-1:
			nbytes /= 1024.
			i += 1
		f = ('{:.2f}'.format(nbytes)).rstrip('0').rstrip('.')
		return '{}{}' .format(f, suffixes[i])

	@staticmethod
	def _runProcess(exe):
		args = shlex.split(exe)
		p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
		for line in p.stdout:
			yield line.decode(encoding='UTF-8').strip('\x00').strip('\n')

		while(True):
			retcode = p.poll() #returns None while subprocess is running
			if retcode is not None:
				if retcode > 0:
					raise ReturnCodeException("{} = {} ! Error".format(exe,retcode))
				else:
					return retcode

	@staticmethod
	def _parseTree(lines):
		tree = {}
		'''
		for item in lines:
			t = tree
			(key,value)=re.split('\s*:\s*',item,2)
			for part in key.split('.'):
					t = t.setdefault(part, {})
			try:
				t = t.setdefault(':',value)
			except AttributeError:
				print("Invalid line %s"%item)	
		'''
		for item in lines:
			(key,value)=re.split('\s*:\s*',item,2)
			tree.setdefault(key,value)
		return tree
	'''
	============================================================================
	Entry point
	============================================================================
	'''
	async def Process(self):
		await self._send_text("Yes my lord.")
		"""Process the command"""
		self.loop = asyncio.get_event_loop()
		try:
			method = getattr(self, self.command)
			if inspect.iscoroutinefunction(method):
				r = await method(self.args)
				await self._send_text(r)
				return
			else:
				r = self.loop.run_in_executor(None, method, self.args)
				#self.store.futures.append(r)
				await self._send_text("My pleasure!")
				return r

		except AttributeError as e:
			LOG.debug(e)
			r = await self.run_ext(self.command)
			await self._send_text(r)

	'''
	Run a configured command an just send back the output without any processing
	'''
	async def run_ext(self, command):
		result = self.message["wrong_command"]
		try:
			exe=self.config.exe[command]
			lines=["<pre>"]
			try:
				for line in self._runProcess(exe):
					LOG.debug(line)
					lines.append(line)
			except ReturnCodeException as e:
					LOG.error(e)
					lines.append(self.message["exception"].format(e))
			except FileNotFoundError as e:
					LOG.error(e)
					lines.append(self.message["exception"].format(e))
			lines.append("</pre>")
			result = "\n".join(lines)
		except Exception as e:
			LOG.debug(e)
			result = "What is your will?"
		finally:
			return result
	'''
	ASYNC methods
	'''
	async def _send_text(self, text):
		if text is not None:
			await self.chat.send_text_to_room(text,room_id=self.room.room_id)

	def _th_send_text(self,text):
		if text is not None:
			asyncio.run_coroutine_threadsafe(self.chat.send_text_to_room(text,room_id=self.room.room_id), self.loop)

	async def _echo(self):
		"""Echo back the command's arguments"""
		response = " ".join(self.args)
		await self.chat.send_text_to_room(response,room_id=self.room.room_id)
	''' 
	===========================
	Actual commands start here
	===========================
	'''
	async def help(self, param):
		return f"{self.config.help_text}"

	async def status(self,param):
		answer = []
		for k,s in self.store.status.items():
			if len(s.progress) > 0:
				answer.append(s.progress)

		r = "Do you have a plan Mister Fix?"
		if self.config.botName == "Amazon":
			r = "Death to all who oppose the Horde!"
		elif self.config.botName == "Sirius":
			r = "Ready to soar, master"
		elif self.config.botName == "Dental":
			r = "Do you have a plan Mister Fix?"
		elif "Virt" in self.config.botName:
			r = await self.run_ext("virsh")
		elif self.config.botName == "Test":
			r = await self.run_ext("virsh")
		answer.append(r)

		if len(answer) > 0:
			return "  \n".join(answer)
		else:
			return None

	async def df(self,args):
		if "psutil" in sys.modules:
			lines=["Disk|Total|Free|Free%","---|---|---|---"]
			exclude={"fdescfs","devfs","nullfs"}
			for part in psutil.disk_partitions():
				if(part.fstype not in exclude):
					u=psutil.disk_usage(part.mountpoint)
					lines.append("{}|{}|{}|{:.0f}%".format(part.mountpoint,self._size(u.total),self._size(u.free),u.free/u.total*100))
			result = "\n".join(lines)
			return result

		else:
			return "\n".join([line for line in self._runProcess("df -h")])

	def raid(self,args):
		r=self.message["wrong_command"]
		exe = None
		result = self.message["wrong_command"]
		try:
			exe=self.config.exe["raid"]
		except Exception:
			pass
		if exe is not None:
			lines=[]
			try:
				for line in self._runProcess(exe):
					LOG.debug(line)
					lines.append(line)
			except  ReturnCodeException as e:
					LOG.error(e)
					lines.append(self.message["exception"].format(e))
			except FileNotFoundError as e:
					LOG.error(e)
					lines.append(self.message["exception"].format(e))
			result = "\n".join(lines)
		self._th_send_text(result)
		return result

	def ups(self,param):
		result="Uuuups..."
		exe = None
		result = self.message["wrong_command"]
		try:
			exe=self.config.exe["ups"]
		except Exception:
			pass
		if exe is not None:
			statusTable={
				'1.  UPS Status is {0[0]}':('ups.status',),
				'2.1 Battery charge {0[0]}':('battery.charge',),
				'2.2 Battery temperature {0[0]}':('battery.temperature',),
				'2.3 Battery voltage {0[0]} of {0[1]}':('battery.voltage','battery.voltage.nominal')
			}
			items=[]
			status=self._parseTree(self._runProcess(exe))
			for statusLine in sorted(statusTable.keys()):
				try:
					statusData=[]
					for statusKey in statusTable[statusLine]:
						statusData.append(status[statusKey])
					items.append(statusLine.format(statusData))
				except KeyError:
					items.append(statusLine.format(statusTable[statusLine]))

			result="\n".join(items)
		self._th_send_text(result)
		return result


	def backup(self,args):
		_status = self.store.status['backup']
		if _status.lock.locked() :
			self._th_send_text(_status.progress)
			return
		exe = None
		try:
			exe=self.config.exe["backup"]
		except Exception:
			pass

		if args is None or len(args) == 0 or exe is None:
			self._th_send_text(self.message["wrong_command"])
			return

		with _status.lock :
			param = " ".join(args)

			_status.progress = f"Backup of **{args[0]}** is started..."
			self._th_send_text( _status.progress)
			try:
				for line in self._runProcess(f"{exe} {param}"):
					_status.progress = line

			except  ReturnCodeException as e:
				LOG.error(e)
				self._th_send_text(self.message["exception"].format(e))

			_status.progress = f"Backup of **{args[0]}** has been finished : **{_status.progress}** at *{datetime.now()}*"
			self._th_send_text(_status.progress)