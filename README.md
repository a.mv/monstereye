# MonsterEye

Monster Eye watcher

1. Install requirements
2. Configure server in `watcher.yaml`
3. Start in interactive of daemon mode

Interactive mode sets log level to debug and output to the stdout
Daemon mode sends logs to syslog

You must invite daemon to the room in order it to be able to join and listen for commands

Sample configuration:

```
# Below you will find various config sections and options
# Default values are shown

default: &default
  PID : "/var/run/MonsterEye/watcher.pid"
  NotifyUsers :
    - "@u1:matrix.example"
    - "@u2:matrix.example"
  room : "Daemons"
  homeserver: "https://matrix.example"
  db_store: "/var/run/MonsterEye/watcher.db"
  pipe: "/var/run/MonsterEye/Pipe"

# Options for connecting to the bot's Matrix account
matrix:
  Test:
    <<: *default
    # The Matrix User ID of the bot account
    user_id: "@test:matrix.example"
    # The device ID given on login
    device_id: testDevice
    db_store: "/tmp/watcher.db"
    pipe: "/tmp/pipe"
    PID : "/tmp/watcher.pid"
    commands:
      raid : raid.sh
      ups : xxx
      backup : backup.pl

# Internal configuration. BE CAREFUL!
internal: &internal
internal: &internal
  command_re: '[hH]ey\s+({})\s+(\w+\s*\w*)*'
  prompt_prefix: "# {}"
  #sync interval in seconds
  sync_interval: 5
  help_text: >
    **Available commands:**<br/>
    help<br/>
    status<br/>
    df<br/>
    ups<br/>
    raid<br/>
    backup *DBname*<br/>

```